//
//  Activities.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import Foundation
//Activities Class is modeled to assign set of predefined activities based on user selected profession.
class Activities{
    var activities:[String] = []
    //updateActivities() gets invoked when the user registers into the application before getting saved into Kinvey.
    func updateActivities(userProfession:String)->[String]{
        if userProfession == "STUDENT" {
            activities = ["Sleep","Study","Eat"]
        }
        else if userProfession == "PROFESSOR"{
            activities = ["Work", "Teach", "Eat","Exercise", "Research"]
        }else if userProfession == "LAWYER"{
            activities = ["Work", "Eat", "Exercise", "Meetings"]
        }else if userProfession == "DOCTOR"{
            activities = ["Teach", "Eat","Exercise", "Hospital"]
        }else if userProfession == "OTHER"{
            activities = ["Eat","Exercise", "Sleep","Study"]
        }
        return activities
    }
}