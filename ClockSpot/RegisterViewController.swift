//
//  RegisterViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/13/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit
//RegisterViewController is associated with Register Scene and it implements UIPickerViewDataSource, UIPickerViewDelegate as Picker is provided in this scene.
class RegisterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate,OperationProtocol{
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var profesionPicker: UIPickerView!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailErrorLBL: UILabel!
    @IBOutlet weak var passwordErrorLBL: UILabel!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    var kinvey:KinveyOperations!
    var activities:Activities!
    var user:User!
    var activityTimeTracker:ActivityTimeTracker!
    var actvitiesWithGoals:GoalModelClass!
    var selectedValue:String!
    //profession is associated with picker for selecting profession, and this array presents the professions that we have chosen to place in picker.
    var profession = ["STUDENT","LAWYER","DOCTOR","PROFESSOR","OTHER"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        kinvey = KinveyOperations(operationProtocol: self)
        activities = Activities()
        activityTimeTracker = ActivityTimeTracker()
        actvitiesWithGoals = GoalModelClass()
        activityTimeTracker.activityTimes = [:]
        actvitiesWithGoals.activityGoal = [:]
        emailErrorLBL.hidden = true
        passwordErrorLBL.hidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "Registration"
    }
    //numberOfComponentsInPickerView, pickerView for numberOfRows in picker and pickerView for the selected component methods should be implemented as it has conform to UIPickerViewDataSource and UIPickerViewDelegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return profession.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return profession[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        // use the row to get the selected row from the picker view
        // using the row extract the value from your datasource (array[row])
        selectedValue = profession[row]
    }
    //registerUser() gets invoked when Register button is clicked and validations are implemented for each of the field.
    @IBAction func registerUser(sender: AnyObject) {
        var valid = true
        if firstNameTF.text!.isEmpty{
            firstNameTF.layer.borderWidth = 1
            firstNameTF.layer.borderColor = UIColor.redColor().CGColor
        }
        else{
            firstNameTF.layer.borderColor = UIColor.whiteColor().CGColor
        }
        if lastNameTF.text!.isEmpty{
            lastNameTF.layer.borderWidth = 1
            lastNameTF.layer.borderColor = UIColor.redColor().CGColor
        }
        else{
            lastNameTF.layer.borderColor = UIColor.whiteColor().CGColor
        }
        if userNameTF.text!.isEmpty{
            userNameTF.layer.borderWidth = 1
            userNameTF.layer.borderColor = UIColor.redColor().CGColor
        }
        else{
            userNameTF.layer.borderColor = UIColor.whiteColor().CGColor
        }
        if emailTF.text!.isEmpty{
            emailTF.layer.borderWidth = 1
            emailTF.layer.borderColor = UIColor.redColor().CGColor
        }
        else{
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            let range = emailTF.text!.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
            let result = range != nil ? true : false
            
            if result{
                valid = true
                emailErrorLBL.hidden = true
                emailTF.layer.borderColor = UIColor.whiteColor().CGColor
            }
            else{
                valid = false
                emailErrorLBL.textColor = UIColor.redColor()
                emailErrorLBL.text = "Please enter a valid Email Id"
                emailErrorLBL.hidden = false
                emailTF.text = ""
                emailTF.layer.borderWidth = 1
                emailTF.layer.borderColor = UIColor.redColor().CGColor
            }
        }
        if passwordTF.text!.isEmpty{
            passwordTF.layer.borderWidth = 1
            passwordTF.layer.borderColor = UIColor.redColor().CGColor
        }
        else{
            let passwordRegEx = "[A-Z0-9a-z.!@#$%^&*()]{6,13}"
            let range = passwordTF.text!.rangeOfString(passwordRegEx, options:.RegularExpressionSearch)
            let result = range != nil ? true : false
            if result{
                valid = true
                passwordErrorLBL.hidden = true
                passwordTF.layer.borderColor = UIColor.whiteColor().CGColor
            }
            else{
                passwordErrorLBL.textColor = UIColor.redColor()
                valid = false
                passwordErrorLBL.text = "Please must be minimum of 6 characters"
                passwordErrorLBL.hidden = false
                passwordTF.text = ""
                passwordTF.layer.borderWidth = 1
                passwordTF.layer.borderColor = UIColor.redColor().CGColor
            }
        }
        if confirmPasswordTF.text!.isEmpty{
            confirmPasswordTF.layer.borderWidth = 1
            confirmPasswordTF.layer.borderColor = UIColor.redColor().CGColor
        }
        else
        {
            let passwordRegEx = "[A-Z0-9a-z.!@#$%^&*()]{6,13}"
            let range = confirmPasswordTF.text!.rangeOfString(passwordRegEx, options:.RegularExpressionSearch)
            let result = range != nil ? true : false
            if result{
                valid = true
                confirmPasswordTF.layer.borderColor = UIColor.whiteColor().CGColor
            }
            else{
                valid = false
                passwordErrorLBL.text = "Password length must be between 6-13\n"
                confirmPasswordTF.text = ""
                confirmPasswordTF.layer.borderWidth = 1
                confirmPasswordTF.layer.borderColor = UIColor.redColor().CGColor
            }
        }
        if passwordTF.text! != confirmPasswordTF.text!{
            displayAlertControllerWithTitleforFailure("Fail!", message:"Your passwords don't match \n")
            passwordTF.text = ""
            confirmPasswordTF.text = ""
            passwordTF.layer.borderWidth = 1
            passwordTF.layer.borderColor = UIColor.redColor().CGColor
            confirmPasswordTF.layer.borderWidth = 1
            confirmPasswordTF.layer.borderColor = UIColor.redColor().CGColor
        }
        //If all the values are entered properly then an instance of User is created with the enetered values and the user is registered.
        if !firstNameTF.text!.isEmpty && !lastNameTF.text!.isEmpty && !userNameTF.text!.isEmpty && !emailTF.text!.isEmpty && !passwordTF.text!.isEmpty && !confirmPasswordTF.text!.isEmpty && valid==true{
            //If any value is not selected from picker, then profession is given as STUDENT by default.
            if selectedValue == nil{
                selectedValue = "STUDENT"
            }
            //An instance of User is instantiated with the values eneterd in the text field and the set of predefined activities are saved into this object based on the selected profession by invoking updateActivities() in Activities Class
            user = User(firstName: firstNameTF.text!, lastName: lastNameTF.text!, username: userNameTF.text!, password: passwordTF.text!, confirmPassword: confirmPasswordTF.text!, profession: selectedValue, email: emailTF.text!)
            user.activitiesValues = activities.updateActivities(user.profession)
            //An instance of ActivityTimeTracker is created(activityTimeTracker) and by default the times tracked while registering are given as 00:00:00
            activityTimeTracker.userName = userNameTF.text!
            for i in 0..<user.activitiesValues.count{
                activityTimeTracker.activityTimes[user.activitiesValues[i]] = "00:00:00"
            }
            //When register is clicked, the values of user and activities along with their activityTimes are saved into Kinvey by passing these values to registerUser() in KinveyOperations.swift
            kinvey.registerUser(user,activityTimeTracker: activityTimeTracker)
        }
    }
    //Once success response is recieved from Kinvey onSuccess() gets invoked. This method invokes an alert to show a success message.
    func onSuccess() {
        displayAlertControllerWithTitle("Success", message:"Registration Successful")
    }
    //Once error response is recieved from Kinvey onError() gets invoked. This method invokes an alert to show an error message.
    func onError(message: String) {
        displayAlertControllerWithTitleforFailure("OOPS!", message:"Registration Failed")
    }
    
    func noActiveUser() {
        //code
    }
    
    func loginFailed() {
        //code
    }
    //displayAlertControllerWithTitle() is invoked from OnSucess() to display a success alert and navigates back to Login page.
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
                self.navigationController?.pushViewController(login, animated: true) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    //displayAlertControllerWithFailure() is invoked from OnError() to display an alert which shows the error message.
    func displayAlertControllerWithTitleforFailure(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    //cancelBTN() gets invoked when Cancel button is invoked on Register Page and navigates back to Login page.
    @IBAction func cancelBTN(sender: AnyObject) {
        let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: true)
    }
}