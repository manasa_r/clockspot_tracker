//
//  FirstViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/12/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import UIKit
//ActivitiesViewController is associated with Activities Tab that gets displayed once the users logs in successfully.
class ActivitiesViewController: UIViewController, OperationProtocol, UITableViewDelegate, UITableViewDataSource  {
    var kinveyOperations:KinveyOperations!
    var activityTimes:[String:String]!
    var activities:[String]!
    var time:String!
    var startTime = NSTimeInterval()
    var timer:NSTimer = NSTimer()
    let defaults = NSUserDefaults.standardUserDefaults()
    var userValue:String!
    var timerLabel:UILabel!
    var subTitleLabel:UILabel!
    var activityTimeTracker:ActivityTimeTracker!
    var convertedDate:String!
    var currentDate = NSDate()
    var dateFormatter = NSDateFormatter()
    //activitiesTableView is associated with table view in Activities tab to display activities in a table
    @IBOutlet weak var activitiesTableView: UITableView!
    var index:NSIndexPath!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Activities"
        kinveyOperations = KinveyOperations(operationProtocol: self)
        //retrieveTime() retieves values from the collection to display actvities with their times
        kinveyOperations.retrieveTime()
        activities = []
        activityTimeTracker = ActivityTimeTracker()
    }
    //As it is a Table View it has to conform to protocols UITableViewDelegate, UITableViewDataSource and this mathod returns number of sections in table view
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if activities != nil{
            return 1
        }else{
            return 0
        }
    }
    //This method returns number of rows in a particular section.
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if activities != nil{
            return activities.count
        }else{
            return 0
        }
    }
    //This method is used to display predefined set of activities along with the tracked times.
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("activities", forIndexPath: indexPath) as UITableViewCell
        let activity:UILabel = cell.viewWithTag(19) as! UILabel
        let activityTime:UILabel = cell.viewWithTag(20) as! UILabel
        var activityValue = activities
        let activityImage:UIImageView = cell.viewWithTag(30) as! UIImageView
        //userValue is the name of the logged in user which is retrived using KCSUserDefaults
        userValue = defaults.valueForKey(Constants.USERNAME) as! String
        activityTimeTracker.userName = userValue
        if activities != nil{
            activity.text = activities[indexPath.row]
            activityTime.text = activityTimeTracker.activityTimes[activities[indexPath.row]]!
            let image: UIImage? = UIImage(named: activityValue[indexPath.row])
            if  image == nil{
                activityImage.image = UIImage(named: "Default.png")
            }else{
                activityImage.image = image!
            }
            //Designed a switch programatically, used to track time for each activity.
            let switchDemo =   UISwitch(frame:CGRectMake(50, 50, 0, 0))
            switchDemo.on = false
            switchDemo.setOn(false, animated: false)
            switchDemo.tag = indexPath.row
            switchDemo.addTarget(self, action: "switchValueDidChange:", forControlEvents: .ValueChanged)
            cell.accessoryView = switchDemo
            cell.contentView.addSubview(switchDemo)
        }
        return cell
    }
    //This method is used to disableAllSwitches except for the selected row when switch is turned on
    func disableAllSwitchesExceptForRow(row:Int){
        for i in 0.stride(to: activities.count, by: 1){
            let currentSwitch  = self.activitiesTableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0))!.accessoryView as! UISwitch
            currentSwitch.enabled = row == currentSwitch.tag
        }            
    }
    //This method is used to enableAllSwitches when switch is turned off of the selected row.
    func enableAllSwitches(){
        for i in 0.stride(to: activities.count, by: 1){
            let currentSwitch  = self.activitiesTableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0))!.accessoryView as! UISwitch
            currentSwitch.enabled = true
        }
    }
    
    // called when the user taps on the sswitch in the row ...
    func switchValueDidChange(sender:UISwitch!)
    {
        if (sender.on){
            disableAllSwitchesExceptForRow(sender.tag)
            if (!timer.valid) {
                let aSelector : Selector = "updateTime"
                timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: aSelector, userInfo: nil, repeats: true)
                startTime = NSDate.timeIntervalSinceReferenceDate()
                let indexPath  = NSIndexPath(forRow: sender.tag, inSection: 0)
                let cellView  = self.activitiesTableView.cellForRowAtIndexPath(indexPath)
                let startLabel = cellView?.viewWithTag(21) as! UILabel
                timerLabel = startLabel
            }
        }
        else{
            enableAllSwitches()
            timer.invalidate()
            let indexPath  = NSIndexPath(forRow: sender.tag, inSection: 0)
            let cellView  = self.activitiesTableView.cellForRowAtIndexPath(indexPath)
            subTitleLabel = cellView?.viewWithTag(20) as! UILabel
            
            //The current tracked time for the activity is stored in timerLabel
            let time = timerLabel.text!.componentsSeparatedByString(":")
            let currentSeconds = Double(time[2])
            let currentMinutes = Double(time[1])
            let currentHours = Double(time[0])
            
            //The tracked time for the activity previously is present in dictionary of ActivityTimeTracker
            let value = activityTimeTracker.activityTimes[activities[indexPath.row]]!
            var previousTime = value.componentsSeparatedByString(":")
            let previousHours = Double(previousTime[0])
            let previousMinutes = Double(previousTime[1])
            let previousSeconds = Double(previousTime[2])
            
            //Total time is calculated based on the current time and previous Time.
            var totalHours = Int(currentHours!+previousHours!)
            var totalMinutes = Int(currentMinutes!+previousMinutes!)
            var totalSeconds = Int(currentSeconds!+previousSeconds!)
            
            if totalSeconds >= 60{
                totalMinutes = totalMinutes+1
                totalSeconds = totalSeconds - 60
            }
            if totalMinutes >= 60{
                totalHours = totalHours + 1
                totalMinutes = totalMinutes - 60
            }
            let modifiedHours = String(format: "%02d", totalHours)
            let modifiedMinutes = String(format: "%02d", totalMinutes)
            let modifiedSeconds = String(format: "%02d", totalSeconds)
            //When the switch is turned off the updated time for a particular activity is displayed in the subTitleLabel and the same is saved backed to dictionary of ActivityTimeTracker.
            subTitleLabel.text = "\(modifiedHours):\(modifiedMinutes):\(modifiedSeconds)"
            activityTimeTracker.activityTimes[activities[indexPath.row]] = "\(modifiedHours):\(modifiedMinutes):\(modifiedSeconds)"
            //Resets back the timer Label to initial value
            timerLabel.text = "00:00:00"
            //The updated values of activities along with their tracked times are saved back to Kinvey by invoking trackTime() in KinveyOperations.
            kinveyOperations.trackTime(activityTimeTracker)
        }
    }
    //When switch is On, the timer starts and this method gets invoked.
    func updateTime(){
        if timerLabel != nil {
            let currentTime = NSDate.timeIntervalSinceReferenceDate()
            //Find the difference between current time and start time.
            var elapsedTime: NSTimeInterval = currentTime - startTime
            //calculate the hours in elapsed time.
            let hours = UInt8(elapsedTime / 3600.0)
            elapsedTime -= (NSTimeInterval(hours) * 3600)
            //calculate the minutes in elapsed time.
            let minutes = UInt8(elapsedTime / 60.0)
            elapsedTime -= (NSTimeInterval(minutes) * 60)
            //calculate the seconds in elapsed time.
            let seconds = UInt8(elapsedTime)
            elapsedTime -= NSTimeInterval(seconds)
            
            let strHours = String(format: "%02d", hours)
            let strMinutes = String(format: "%02d", minutes)
            let strSeconds = String(format: "%02d", seconds)
            //The timer value is simultaneously displayed in the timerLabel for every second.
            timerLabel.text = "\(strHours):\(strMinutes):\(strSeconds)"
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if activityTimeTracker.activityTimes != nil{
            self.activitiesTableView.reloadData()
        }
    }
    
    //Adding a new activity when + is clicked on Activities tab and reloading the table view
    @IBAction func addActivity(sender: AnyObject) {
        var alertController:UIAlertController?
        //Displays an alert controller with a text field to enter activity name and provides two buttons Submit and Cancel to add or cancel adding this activity.
        alertController = UIAlertController(title: "Add an Activity",
                                            message: "Enter Activity Name",
                                            preferredStyle: .Alert)
        alertController!.addTextFieldWithConfigurationHandler(
            {(textField: UITextField!) in
                textField.placeholder = "Activity Name"
        })
        //Added activity for user is appended to existing activities and time is set to 00:00:00
        let action = UIAlertAction(title: "Submit",
                                   style: UIAlertActionStyle.Default,
                                   handler: {[weak self]
                                    (paramAction:UIAlertAction!) in
                                    if let textFields = alertController?.textFields{
                                        let theTextFields = textFields as [UITextField]
                                        let enteredText = theTextFields[0].text
                                        self!.activities.append(enteredText!)
                                        self?.activityTimeTracker.activityTimes[enteredText!] = "00:00:00"
                                    }
                                    //trackTime() is called to save the updated activities list in to Kinvey and reloads the table view.
                                    self!.kinveyOperations.trackTime(self!.activityTimeTracker)
                                    self!.activitiesTableView?.reloadData()
            })
        
        let cancel = UIAlertAction(title: "Cancel",
            style: .Cancel,
            handler: {(action) -> Void in
            })

        
        alertController?.addAction(action)
        alertController?.addAction(cancel)
        self.presentViewController(alertController!,
                                   animated: true,
                                   completion: nil)
        
    }
    //called when success response is recieved from retrieving values from Kinvey.
    func sendTimeValues(time: ActivityTimeTracker) {
        for activity in time.activityTimes.keys{
            activities.append(activity)
        }
        if activityTimeTracker.activityTimes == nil {
            activityTimeTracker.activityTimes = [:]
        }
        activityTimeTracker.activityTimes = time.activityTimes
        self.activitiesTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func onError(message: String) {
        // displayAlertControllerWithFailure("OOPS!", message:"Login Failed")
        
    }
    func onSuccess() {
        //displayAlertControllerWithTitle("Success", message:"Login Successful")
    }
    func noActiveUser() {
        //
    }
    
    func loginFailed() {
        //
    }
    
    
}

