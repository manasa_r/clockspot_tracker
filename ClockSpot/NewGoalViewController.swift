//
//  NewGoalViewController.swift
//  ClockSpot
//
//  Created by Pinky on 4/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit
// NewGoalViewController to add activity and its Goal.
class NewGoalViewController: UIViewController, OperationProtocol {
    @IBOutlet weak var goalTimeTF: UITextField!
    @IBOutlet weak var activityNameTF: UITextField!
    var kinveyOperations:KinveyOperations!
    let defaults = NSUserDefaults.standardUserDefaults()
    var activityTimes:[String:String] = [:]
    var goalVC:GoalViewController!
    var activities:[String] = []
    var activityIsPresent:Bool = false
    var activitiesGoal:GoalModelClass!
    var activityGoal:[String:String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activitiesGoal = GoalModelClass()
        activitiesGoal.activityGoal = [:]
        kinveyOperations = KinveyOperations(operationProtocol: self)
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(animated: Bool) {
        //retrieves activities and  tehir times from collection.
        kinveyOperations.retrieveTime()
    }
    //called when save button is clicked after entering values in NewGoalViewController. Displays an alert if activity is not present in Activities Tab and if it is present it appends the added activity to the list and updates back to the collection in Kinvey
    @IBAction func saveBTN(sender: AnyObject) {
        for act in activityTimes.keys{
            activities.append(act)
        }
        for activity in activities{
            if activityNameTF.text != activity{
                activityIsPresent = false
            }else{
                activityIsPresent = true
                break
            }
        }
        if activityIsPresent{
            activitiesGoal.userName = defaults.valueForKey(Constants.USERNAME) as! String
            activitiesGoal.activityGoal[activityNameTF.text!] = goalTimeTF.text!
            kinveyOperations.trackGoal(activitiesGoal)
            
        }else{
            displayAlertControllerWithTitle("OOPS!", message:"Please add this activity before you set the goal")
        }
    }
    //to save the retrieved values into object
    func sendActivityGoals(goal: GoalModelClass) {
        activitiesGoal.activityGoal = goal.activityGoal
    }
    
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    
    func onError(message: String) {
        // code
    }
    //once goal is added successfully it navigates back to GoalViewController.
    func onSuccess() {
        let goal =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("goal") as! GoalViewController
        self.navigationController?.pushViewController(goal, animated: true)
    }
    func noActiveUser() {
        //code
    }
    func loginFailed() {
        //code
    }
    
    func sendValues(user: User) {
        //code
    }
    //to save the retrieved values into the object
    func sendTimeValues(time: ActivityTimeTracker) {
        self.activityTimes =  time.activityTimes
        //retrieve goals from collection
        kinveyOperations.retrieveGoals()
    }
    //called when cancel button is clicked.
    @IBAction func cancelBTN(sender: AnyObject) {
        let backToGoalView =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("goal") as! GoalViewController
        self.navigationController?.pushViewController(backToGoalView, animated: true)
    }
    
}
