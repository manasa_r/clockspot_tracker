//
//  Login.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import Foundation
//Login class is modeled to store userName and password and use this object to validate against the user in Kinvey.
class Login {
    var userName:String
    var password:String
    
    init(userName:String, password:String){
        self.userName = userName
        self.password = password
    }
}