//
//  User.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/13/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import Foundation
//User class corresponds to the values that we enter in Sign up page.
class User :NSObject {
    var firstName:String!
    var lastName:String!
    var username:String!
    var password:String!
    var confirmPassword:String!
    var profession:String!
    var email:String!
    var activitiesValues:[String]!
    var entityId:String?
    
    init(firstName:String, lastName:String, username:String, password:String, confirmPassword:String, profession:String, email:String){
        self.firstName = firstName
        self.lastName = lastName
        self.username = username
        self.password = password
        self.confirmPassword = confirmPassword
        self.profession = profession
        self.email = email
    }
    override init() {
        super.init()
    }
    init(username:String)
    {
        self.username = username
    }
    init(email:String)
    {
        self.email = email
    }
    init(password:String)
    {
        self.password = password
    }
    init(confirmPassword:String)
    {
        self.confirmPassword = confirmPassword
    }
    init(profession:String)
    {
        self.profession = profession
    }
    //hostToKinveyPropertyMapping() maps these values to the collection in Kinvey.
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "firstName" : "first_name",
            "lastName" : "last_name",
            "username" : "username",
            "password" : "password",
            "profession" : "profession",
            "email" : "email",
            "activitiesValues":"activitiesValues"
        ]
    }
}