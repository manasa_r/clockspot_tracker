//
//  LoginViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/13/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit
//LoginViewController is associated with Login Scene.
class LoginViewController: UIViewController, OperationProtocol {
    //userNameTF is associated with the userName textfield in Login Scene
    @IBOutlet weak var userNameTF: UITextField!
    //passwordTF is associated with the password textfield in Login Scene
    @IBOutlet weak var passwordTF: UITextField!
    var kinveyOperations:KinveyOperations!
    var login:Login!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        kinveyOperations = KinveyOperations(operationProtocol: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //login() is invoked when login button is clicked in the login scene
    @IBAction func login(sender: AnyObject) {
        login = Login(userName: userNameTF.text!, password: passwordTF.text!)
        //The values entered by user will be passed to loginUser() which validates the userName and password with the values stored in Kinvey.
        kinveyOperations.loginUser(login)
    }
    //displayAlertControllerWithTitle() is invoked from OnSucess() to display a success alert and navigates to Tabbed application.
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in self.performSegueWithIdentifier("success", sender: self) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    //displayAlertControllerWithFailure() is invoked from OnError() to display an alert which shows the error message.
    func displayAlertControllerWithFailure(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    //called when Forgot password is clicked and it navigates to ForgotPasswordViewController to update the password.
    @IBAction func forgotPaswwordBTN(sender: AnyObject) {
        let forgotPaswwordView =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("forgotPassword") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotPaswwordView, animated: true)
    }
    //Once error response is recieved from Kinvey onError() gets invoked. This method invokes an alert to show an error message.
    func onError(message: String) {
        displayAlertControllerWithFailure("OOPS!", message:"Login Failed")
        
    }
    //Once success response is recieved from Kinvey onSuccess() gets invoked. This method invokes an alert to show a success message.
    func onSuccess() {
        displayAlertControllerWithTitle("Success", message:"Login Successful")
    }
    func noActiveUser() {
        //
    }
    func loginFailed() {
        //
    }

}
