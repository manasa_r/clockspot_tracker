//
//  KCSUserExtension.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import Foundation
let userType = "profession"
let activities = "activitiesValues"
let username = "username"
let activityTime = "activityTimes"
//This gives the ability to extend types to map it to kinvey.
extension KCSUser{
    var role:String {
        return self.getValueForAttribute(userType) as! String!
    }
    var activityValues:[String] {
        return self.getValueForAttribute(activities) as! [String]!
    }
    var user:String {
        return self.getValueForAttribute(username) as! String!
    }
    var timeOfActivities:[String:String] {
        return self.getValueForAttribute(activityTime) as! [String:String]!
    }
    
}

