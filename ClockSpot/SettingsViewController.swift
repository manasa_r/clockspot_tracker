//
//  SettingsViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/12/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import UIKit
//SettingsViewController is associated with settings tab
class SettingsViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.whiteColor()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //called when update button is clicked on settings view and navigates to UpdateProfileViewController
    @IBAction func updateProfileBTN(sender: AnyObject) {
        let toProfile =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("profile") as! UpdateProfileViewController
        self.navigationController?.pushViewController(toProfile, animated: true)
        
        
    }
}