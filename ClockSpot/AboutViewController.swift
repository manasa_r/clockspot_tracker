//
//  AboutViewController.swift
//  ClockSpot
//
//  Created by Karupakala,Rajesh on 3/15/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import UIKit
//AboutViewController is to display infomation about our application.
class AboutViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "About"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
