//
//  UpdateProfileViewController.swift
//  ClockSpot
//
//  Created by Karupakala,Rajesh on 3/15/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit
//UpdateProfileViewController to deal with updating the uer profile
class UpdateProfileViewController: UIViewController, OperationProtocol {
    var user:User!
    var kinveyOperations:KinveyOperations!
    var firstName:String!
    var lastName:String!
    var userName:String!
    var profession:String!
    var email:String!
    var password:String!
    var confirmPassword:String!
    var activities:[String]!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        kinveyOperations = KinveyOperations(operationProtocol: self)
        //retrieves the user profile form collection
        kinveyOperations.retrieveForSettings()
    }
    //called when save button is clicked.
    @IBAction func saveBTN(sender: AnyObject) {
        firstName = firstNameTF.text
        lastName = lastNameTF.text
        userName = user.username
        profession = user.profession
        email = user.email
        password = user.password
        confirmPassword = user.confirmPassword
        activities = user.activitiesValues
        user = User(firstName: firstName, lastName: lastName, username: userName, password: "", confirmPassword: "", profession: profession, email: email)
        user.activitiesValues = activities
        //calls saveUpdatedProfile to update the user profile
        kinveyOperations.saveUpdatedProfile(user)
        displayAlertControllerWithTitle("Success", message: "Profile has been updated")
    }
    func onError(message: String) {
        
    }
    func onSuccess() {
        
    }
    func noActiveUser() {
        
    }
    
    func loginFailed() {
        
    }
    
    func sendValues(user:User) {
        
    }
    
    func sendTimeValues(time:ActivityTimeTracker){
        
    }
    //saves the retrieved values into user object and the text fields present.
    func retrieveProfile(user:User){
        self.user = user
        firstNameTF.text = user.firstName
        lastNameTF.text = user.lastName
    }
    //displayAlertControllerWithTitle() is invoked to display a success alert and navigates to Settings tab.
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            
            handler:  { action in let settings =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("settings") as! SettingsViewController
                self.navigationController?.pushViewController(settings, animated: true) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
}
