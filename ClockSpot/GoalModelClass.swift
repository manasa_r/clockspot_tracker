//
//  GoalModelClass.swift
//  ClockSpot
//
//  Created by Pinky on 4/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import Foundation
//GoalModelClass is modeled to save userName, activityNames along with the set goal values.
class GoalModelClass:NSObject{
    var entityId:String?
    var userName:String!
    var activityGoal:[String:String]!
    init(userName:String, activityGoal:[String:String]){
        self.userName = userName
        self.activityGoal = activityGoal
    }
    override init(){
        super.init()
    }
    //hostToKinveyPropertyMapping() maps these values to the collection in Kinvey.
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "userName" : "userName",
            "activityGoal":"activityGoal"
        ]
    }
}