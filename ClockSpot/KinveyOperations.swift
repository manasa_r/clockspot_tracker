//
//  KinveyOperations.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/14/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import Foundation
@objc protocol OperationProtocol {
    func onSuccess()
    func onError(message:String)
    func noActiveUser()
    func loginFailed()
    optional func sendValues(user:User)
    optional func sendTimeValues(time:ActivityTimeTracker)
    optional func retrieveProfile(user:User)
    optional func sendActivityGoals(goal:GoalModelClass)
    
}
//Kinveyoperations hold all activities related to Kinvey. All oprations saving,fetching,deleting in Kinvey are implemented here.
class KinveyOperations {
    //store is to represent ClockSpotUsers collection in Kinvey
    let store:KCSAppdataStore!
    //timeStore is to represent ActivitiesTime collection in Kinvey
    let timeStore:KCSAppdataStore!
    //activityGoal is to represent ActivityGoal collection in Kinvey
    let activityGoal:KCSAppdataStore!
    let operaionDelegate:OperationProtocol!
    let defaults = NSUserDefaults.standardUserDefaults()
    init(operationProtocol:OperationProtocol){
        self.operaionDelegate = operationProtocol
        store = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ClockSpotUsers",
            KCSStoreKeyCollectionTemplateClass : User.self
            ])
        timeStore = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ActivitiesTime",
            KCSStoreKeyCollectionTemplateClass : ActivityTimeTracker.self
            ])
        activityGoal = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "ActivityGoal",
            KCSStoreKeyCollectionTemplateClass : GoalModelClass.self
            ])
    }
    func saveData() {
        if let _ = KCSUser.activeUser() {
        }else{
            operaionDelegate.noActiveUser()
        }
    }
    //called from RegisterViewController to save the user info into "ClockSpotUsers" and activityTimeTracker info into "ActivitiesTime" collection in Kinvey
    func registerUser(user:User, activityTimeTracker:ActivityTimeTracker){
        let userRows  = [
            activities : user.activitiesValues,
            userType : user.profession,
            KCSUserAttributeGivenname : user.firstName,
            KCSUserAttributeSurname : user.lastName,
            KCSUserAttributeEmail : user.email,
            ]
        KCSUser.userWithUsername(
            user.username,
            password:user.password,
            fieldsAndValues: userRows as [NSObject : AnyObject],
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operaionDelegate.onSuccess()
                    self.store.saveObject(
                        user,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                //save failed
                            self.operaionDelegate.onError("something gone wrong!")
                            } else {
                                //save was successful
                            self.operaionDelegate.onSuccess()
                            }
                        },
                        withProgressBlock: nil
                    )
                    //call saveActivitiesAndTimeAfterSuccesfulRegistration() to save the activities and their tracked times into the collection
                    self.saveActivitiesAndTimeAfterSuccesfulRegistration(activityTimeTracker)
                } else {
                    self.operaionDelegate.onError(errorOrNil.description)
                }
            }
        )
        saveData()
    }
    //called from LoginViewController to validate the user and set the value for Constants.USERNAME with the userName of the logged in User.
    func loginUser(userLogin:Login){
        KCSUser.loginWithUsername(
            userLogin.userName,
            password: userLogin.password,
            withCompletionBlock: { ( user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operaionDelegate.onSuccess()
                    self.defaults.setValue(userLogin.userName, forKey: Constants.USERNAME)
                    self.defaults.synchronize()
                    self.operaionDelegate.onSuccess()
                } else {
                    self.operaionDelegate.onError("OOPS!")
                }
            }
        )
    }
    //called when user clicks on register button to save the activityTime values into "ActivitiesTime"
    func saveActivitiesAndTimeAfterSuccesfulRegistration(activityTimeTracker:ActivityTimeTracker){
        
        timeStore.saveObject(
            activityTimeTracker,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil != nil {
                    //save failed
                    self.operaionDelegate.onError("something gone wrong!")
                } else {
                    //save was successful
                    self.operaionDelegate.onSuccess()
                }
            },
            withProgressBlock: nil
        )
    }
    //called from ActivitiesViewController to save the updated activities and their times by deleting the previous record for that user and saving this new object in "ActivitiesTime" Collection
    func trackTime(time:ActivityTimeTracker){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "userName", withExactMatchForValue: userValue)
        timeStore.removeObject(
            query,
            withDeletionBlock: { (deletionDictOrNil: [NSObject : AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    //error occurred - add back into the list
                    NSLog("Delete failed, with error: %@", errorOrNil.localizedFailureReason!)
                } else {
                    //delete successful - UI already updated
                    NSLog("deleted response: %@", deletionDictOrNil)
                    self.timeStore.saveObject(
                        time,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                //save failed
                                self.operaionDelegate.onError("something gone wrong!")
                            } else {
                                //save was successful
                                self.operaionDelegate.onSuccess()
                            }
                        },
                        withProgressBlock: nil
                    )
                }
            },
            withProgressBlock: nil
        )
    }
    //called to retrieve the values from "ActivitiesTime" Collection.
    func retrieveTime(){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "userName", withExactMatchForValue: userValue)
        timeStore.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil == nil{
                    let userObject =   objectsOrNil[0] as! ActivityTimeTracker
                    self.operaionDelegate.sendTimeValues!(userObject)
                }
            },
            withProgressBlock: nil
        )
    }
    //called to retrieve the goals from "ActivityGoal" to retrieve the set goals.
    func retrieveGoals(){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "userName", withExactMatchForValue: userValue)
        activityGoal.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil == nil{
                    
                    if objectsOrNil.count == 0 {
                        self.operaionDelegate.onError("OOPS!")
                        return
                    }
                    let userObject =   objectsOrNil[0] as! GoalModelClass
                    self.operaionDelegate.sendActivityGoals!(userObject)
                }
            },
            withProgressBlock: nil
        )
    }
    //called from UpdateProfileViewController to remove the existing record of the user and save this updated object in "ClockSpotUsers" collection.
    func saveUpdatedProfile(user:User){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "username", withExactMatchForValue: userValue)
        //deleting values of logged in user.
        store.removeObject(
            query,
            withDeletionBlock: { (deletionDictOrNil: [NSObject : AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    //error occurred - add back into the list
                    NSLog("Delete failed, with error: %@", errorOrNil.localizedFailureReason!)
                } else {
                    //delete successful - UI already updated
                    self.store.saveObject(
                        user,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                //save failed
                                self.operaionDelegate.onError("something gone wrong!")
                            } else {
                                //save was successful
                                self.operaionDelegate.onSuccess()
                            }
                        },
                        withProgressBlock: nil
                    )
                    NSLog("deleted response: %@", deletionDictOrNil)
                }
            },
            withProgressBlock: nil
        )
    }
    
    //called to retrieve the values form "ClosSpotUsers" collection.
    func retrieveForSettings(){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "username", withExactMatchForValue: userValue)
        store.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil == nil{
                    let userObject =   objectsOrNil[0] as! User
                    self.operaionDelegate.retrieveProfile!(userObject)
                }
            },
            withProgressBlock: nil
        )
    }
    //called from NewGoalViewController to remove the existing record for that particular user and to save this new object in "ActivityGoal"
    func trackGoal(goal:GoalModelClass){
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "userName", withExactMatchForValue: userValue)
        //deleting values of logged in user.
        activityGoal.removeObject(
            query,
            withDeletionBlock: { (deletionDictOrNil: [NSObject : AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    //error occurred - add back into the list
                    NSLog("Delete failed, with error: %@", errorOrNil.localizedFailureReason!)
                } else {
                    NSLog("deleted response: %@", deletionDictOrNil)
                    //delete successful - UI already updated
                    self.activityGoal.saveObject(
                        goal,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                //save failed
                                self.operaionDelegate.onError("something gone wrong!")
                            } else {
                                //save was successful
                                self.operaionDelegate.onSuccess()
                            }
                        },
                        withProgressBlock: nil
                    )
                    
                }
            },
            withProgressBlock: nil
        )
    }
}