//
//  ForgotPasswordViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 4/19/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import UIKit
//ForgotPasswordViewController is associated with Forgot password? in Login View.
class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var emailTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.whiteColor()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //called when submit button is clicked on submit button in Forgot password view. It sends reset link to change password to the registered email which we enter in the text field
    @IBAction func submitBTN(sender: AnyObject) {
        let emailID = emailTF.text!
        let validEmailID:Bool = isValidEmail(emailTF.text!)
        if emailID == "" || validEmailID == false{
            self.displayAlertControllerWithFailure("OOPS!", message: " Please enter emailID ")
        }
        KCSUser.sendPasswordResetForUser(emailID, withCompletionBlock: { (succeeded: Bool, error: NSError?) -> Void in
            if error == nil {
                if succeeded { // SUCCESSFULLY SENT TO EMAIL
                    self.displayAlertControllerWithTitle("Successful", message: "Reset email sent to your inbox : \n \(emailID)")
                }
                else { // SOME PROBLEM OCCURED
                    self.displayAlertControllerWithFailure("Forgot Password", message: "something gone wrong")
                }
            }
            else {
                //ERROR OCCURED, DISPLAY ERROR MESSAGE
                self.displayAlertControllerWithFailure("OOPS", message: error!.description)
            }
        });
    }
    //displayAlertControllerWithTitle() is called when it is successful to display a success alert and navigates to Login View.
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
                self.navigationController?.pushViewController(login, animated: true) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    //displayAlertControllerWithFailure() is called when something goes wrong to display an alert which shows the error message.
    func displayAlertControllerWithFailure(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    //validations for email to be entered in email field
    func isValidEmail(username:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(username)
        
    }
}
