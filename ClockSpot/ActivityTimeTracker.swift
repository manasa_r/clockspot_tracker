//
//  ActivityTimeTracker.swift
//  ClockSpot
//
//  Created by Manasa Bojja on 09/04/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import Foundation
//ActivityTimeTracker is modeled to track the activities along with their times for a particular user.
class ActivityTimeTracker:NSObject{
    var entityId:String?
    var userName:String!
    //activityTimes is a dictionary which stores the activityTimes as key and their tracked times as values.
    var activityTimes:[String:String]!
    init(userName:String, activityTimes:[String:String]){
        self.userName = userName
        self.activityTimes = activityTimes
    }
    override init() {
        super.init()
    }
    //hostToKinveyPropertyMapping() maps these values to the collection in Kinvey.
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "userName" : "userName",
            "activityTimes":"activityTimes"
        ]
    }
}