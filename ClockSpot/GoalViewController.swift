//
//  SecondViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/12/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//

import UIKit
// GoalViewController to show the progress of selected activity
class GoalViewController: UIViewController ,OperationProtocol,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var showProgressTF: UILabel!
    @IBOutlet weak var goalTableView: UITableView!
    @IBOutlet weak var displayTextTF: UILabel!
    var activityTimes:[String:String] = [:]
    var activityNames:[String]!
    var activityGoals:[String:String]!
    var kinveyOperations:KinveyOperations!
    var activityGoal:GoalModelClass!
    var activityTimeTracker:ActivityTimeTracker!
    var time:[String]!
    var activity:[String]!
    
    override func viewDidLoad() {
        self.navigationItem.title = "Goals"
        super.viewDidLoad()
        activityNames = []
        time = []
        activity = []
        activityGoal = GoalModelClass()
        activityTimeTracker = ActivityTimeTracker()
        kinveyOperations = KinveyOperations(operationProtocol: self)
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    // This Method hides the table view if thier are no Goals and sets the Label to No Goals are Set.
    func sendActivityGoals(goal: GoalModelClass) {
        for activity in goal.activityGoal.keys{
            activityNames.append(activity)
        }
        if goal.activityGoal == nil {
            goal.activityGoal = [:]
        }
        activityGoal.activityGoal = goal.activityGoal
        if activityNames == nil{
            displayTextTF.text = "No Goals are Set"
            self.goalTableView.hidden = true
        }else{
            displayTextTF.text = ""
            self.goalTableView.hidden = false
        }
        self.goalTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        kinveyOperations.retrieveTime()
        activityNames = []
    }
    //Table view number of sections
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if activityNames != nil{
            return 1
        }
        return 0
        
    }
    // Table view number of rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if activityNames.count != 0{
            return activityNames.count
        }
        return 0
    }
    // Table view data to display activities for which goals are set along with the set goal time and the tracked time for a particular activity
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellGoal", forIndexPath: indexPath) as UITableViewCell
        let activities:UILabel = cell.viewWithTag(505) as! UILabel
        let activityImage:UIImageView = cell.viewWithTag(509) as! UIImageView
        let goalTime:UILabel = cell.viewWithTag(508) as! UILabel
        let activityTime:UILabel = cell.viewWithTag(507) as! UILabel
        let progressViewBar:UIProgressView = cell.viewWithTag(600) as! UIProgressView
        let image: UIImage? = UIImage(named: activityNames[indexPath.row])
        activities.text = activityNames[indexPath.row]
        if  image == nil{
            activityImage.image = UIImage(named: "Default.png")
        }else{
            activityImage.image = image!
        }
        goalTime.text = activityGoal.activityGoal[activityNames[indexPath.row]]
        activityTime.text = activityTimeTracker.activityTimes[activityNames[indexPath.row]]
        let goalAfterEvaluatingHHMMSS = String(getActivitiesTimes(goalTime.text!))
        let actualTime = String( getActivitiesTimes(activityTime.text!))
        progressViewBar.progress = Float(actualTime)! / Float(goalAfterEvaluatingHHMMSS)!
        if progressViewBar.progress == 1.0{
            displayAlertControllerWithGoal("Hurray", message:"You reached your goal ")
        }
        return cell
    }
    // Convert time format from HH:MM:SS to Minutes
    func getActivitiesTimes(var timeHHMMSS:String) -> Double{
        if timeHHMMSS == ""{
            timeHHMMSS = "00:00:00"
        }
        var sumTime:Double!
        let seperateTime = timeHHMMSS.componentsSeparatedByString(":")
        let hours = Double(Int(seperateTime[0])! * 60)
        let sec = Double(Double(seperateTime[2])! / 60)
        sumTime = Double(hours + Double(seperateTime[1])! + sec)
        return sumTime
    }
    // Navigation to add activity and its Goal in NewGoalViewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goalSegue"
        {
            if let destinationVC = segue.destinationViewController as? NewGoalViewController{
                destinationVC.goalVC = self
            }
        }
    }
    //Message if the Goal is reached
    func displayAlertControllerWithGoal(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    
    
    func onError(message: String) {
        displayTextTF.text = "No Goals are Set"
        self.goalTableView.hidden = true
        
    }
    func onSuccess() {
        //
    }
    func noActiveUser() {
        //
    }
    
    func loginFailed() {
        //
    }
    
    // retrieving updated values from activity tab through Kinvey
    func sendTimeValues(time: ActivityTimeTracker) {
        if activityTimeTracker.activityTimes == nil {
            activityTimeTracker.activityTimes = [:]
        }
        activityTimeTracker.activityTimes = time.activityTimes
        kinveyOperations.retrieveGoals()
    }
}

