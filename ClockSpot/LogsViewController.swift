//
//  LogsViewController.swift
//  ClockSpot
//
//  Created by Bojja,Manasa on 3/12/16.
//  Copyright © 2016 Bojja,Manasa. All rights reserved.
//
import UIKit
// Charts framework to access PieChartView
import Charts
//Logs view controller to display PieChart
class LogsViewController: UIViewController, OperationProtocol {
    @IBOutlet var chartTapGesture: UITapGestureRecognizer!
    @IBOutlet weak var pieChartView: PieChartView!
    var activityTimeTracker:ActivityTimeTracker!
    var activityTimes:[String:String] = [:]
    var activity:[String]!
    var hours:[Double]!
    var timeHHMMSS:[String]!
    var times:[Double]!
    var kinveyOperations:KinveyOperations!
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyOperations = KinveyOperations(operationProtocol: self)
        activity = []
        timeHHMMSS = []
        hours = [20.0, 4.0, 6.0]
        times = []
        //Data displayed when there is no activity started
        pieChartView.noDataTextDescription = " Start an Activity"
        pieChartView.noDataText = " "
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //create pie chart view using activities and thier times
    func setChart(dataPoints: [String], value: [Double]) {
        var dataEntries: [ChartDataEntry] = []
        var activityNames:[String] = []
        // sum all timings
        var sumValues:Double = 0.0
        for i in 0 ..< value.count  {
            sumValues += value[i]
        }
        
        
        if sumValues != 0.0 {
            //Find percentage of each time
            var values:[Double] = []
            for var i = 0; i < value.count ; i+=1 {
                values.append((((value[i] * 100)/sumValues)))
            }
           // get activities whose times are started
            for i in 0..<dataPoints.count {
                if values[i] != 0{
                    let dataEntry = ChartDataEntry(value: Double(values[i]), xIndex: i)
                    activityNames.append(dataPoints[i])
                    dataEntries.append(dataEntry)
                }
            }
            let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "Activity")
            pieChartView.descriptionText = ""
            pieChartDataSet.colors = ChartColorTemplates.colorful()
            let pieChartData = PieChartData(xVals: activityNames, dataSet: pieChartDataSet)
            pieChartView.data = pieChartData
            pieChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .EaseInBounce)
            pieChartView.gestureRecognizerShouldBegin(chartTapGesture)
        }
        else{
            pieChartView.noDataTextDescription = " Start an Activity"
        }
    }
    func onError(message: String) {
        //
        
    }
    func onSuccess() {
        //
    }
    func noActiveUser() {
        //
    }
    
    func loginFailed() {
        //
    }
    //retreive activities and times from Activity Tab through Kinvey
    func sendTimeValues(time: ActivityTimeTracker) {
        self.activityTimes =  time.activityTimes
        getActivitiesTimes()
    }
    //This method gets the activities and times from the dictionary variable of activityTimes and converts the time format HH:MM:SS to minutes.
    func getActivitiesTimes(){
        activity = []
        timeHHMMSS = []
        times = []
        for act in activityTimes.keys{
            activity.append(act)
        }
        for time in activityTimes.values{
            timeHHMMSS.append(time)
        }
        for i in 0 ..< activityTimes.values.count {
            let seperateTime = timeHHMMSS[i].componentsSeparatedByString(":")
            let hours = Double(Double(seperateTime[0])! * 60)
            let sec = Double(Double(seperateTime[2])! / 60)
            let sumTime = Double(hours + Double(seperateTime[1])! + sec)
            times.append(sumTime)
        }
        // call setChart() to draw the PieChart
        setChart(activity, value:( times))
    }
    // This method retrives time when view appears.
    override func viewWillAppear(animated: Bool) {
        kinveyOperations.retrieveTime()
        
    }
}
