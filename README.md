# README #

This README would help you get to know whatever steps are necessary to get ClockSpot application up and running.

###ClockSpot###

* ClockSpot App is developed by a team named TickTock Trackers:
                 1. Manasa Bojja
                 2. Priyanka Anjuri
                 3. Rajesh Karupakala
                 4. Anvesh Reddy Kankanala

* It lets you track the time for activities and look at the logs to see how much time is spent on activities.
* It lets you manage your time effectively


### Running the Application ###

* Checkout the code from the repository.
* Run the application in iPad configuration
* Once the application launches successfully, you could sign up into the application and login.
* Once you are logged in successfully, you can see set of activities displayed.
* You can track the activities, set the goals in Goals Tab, look at the logs in Logs tab and can update your profile in Profile tab. 

### Contribution guidelines ###

* Unit testing on the application is done so that the flow is run smoothly.
* Peer code review was done by team members so that we follow major guidelines for coding.


### Technologies Used ###

* We have developed this application in Swift 2.0
* We used Kinvey as our backend database in order to save, retrieve, and fetch the data.